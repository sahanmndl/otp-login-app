import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import React from "react";
import ProfileView from "../views/main/ProfileView";
import DiscoverView from "../views/main/DiscoverView";
import ChatView from "../views/main/ChatView";
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const BottomTabs = createMaterialBottomTabNavigator();

function BottomNavigator() {
    return (
        <BottomTabs.Navigator
            initialRouteName="ProfileView"
            activeColor={"coral"}
            labeled={false}
            shifting={false}
            barStyle={{ backgroundColor: 'white' }}
        >
            <BottomTabs.Screen
                name="ProfileView"
                component={ProfileView}
                options={{
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons
                            name="account"
                            color={color}
                            size={26}
                        />
                    ),
                }}
                
            />
            <BottomTabs.Screen
                name="DiscoverView"
                component={DiscoverView}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Feather
                            name="search"
                            color={color}
                            size={26}
                        />
                    ),
                }}
            />
            <BottomTabs.Screen
                name="ChatView"
                component={ChatView}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Ionicons 
                            name="chatbubbles" 
                            color={color} 
                            size={26} 
                        />
                    ),
                }}
            />
        </BottomTabs.Navigator>
    )
}

export default BottomNavigator
