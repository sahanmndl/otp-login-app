import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import OTPVerificationView from "../views/OTPVerificationView";
import PhoneNumberView from "../views/PhoneNumberView";
import MainNavigator from "./MainNavigator";

const Stack = createStackNavigator()

function AuthNavigator() {
    return (
        <Stack.Navigator initialRouteName="PhoneNumberView">
            <Stack.Screen
                name="PhoneNumberView"
                component={PhoneNumberView}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name="OTPVerificationView"
                component={OTPVerificationView}
                options={{headerTitle: "Verify Your Phone Number"}}
            />
            <Stack.Screen
                name="MainNavigator"
                component={MainNavigator}
                options={{headerShown: false}}
            />
        </Stack.Navigator>
    )
}

export default AuthNavigator