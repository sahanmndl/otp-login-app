import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { TouchableOpacity } from "react-native";
import { ActivityIndicator, Text } from "react-native";
import { StyleSheet, View } from "react-native";
import OTPTextView from "react-native-otp-textinput";
import Toast from 'react-native-toast-message';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';

const OTPVerificationView = () => {

    const navigation = useNavigation()
    const [otp, setOtp] = useState('')
    const [loading, setLoading] = useState(false)

    function randomBoolean() {
        return Math.random() >= 0.5;
    }

    const handleSubmission = async () => {
        if(otp.trim().length < 4) {
            Toast.show({
                type: 'error',
                text1: 'Please enter valid OTP!',
            })
        } else {
            try {
                const sessionId = await AsyncStorage.getItem('sessionId')
                if(randomBoolean() && sessionId !== null) {
                    var res = "verified"
                    await AsyncStorage.setItem('verified', JSON.stringify(res))
                        .then(() => navigation.reset({
                            index: 0,
                            routes: [{name: 'MainNavigator'}]
                        }))
                        .catch((e) => console.log(e))
                } else {
                    Toast.show({
                        type: 'error',
                        text1: 'Invalid OTP!',
                    })
                }
            } catch(e) {
                console.log(e)
            }
        }
    }
    
    return (
        <View style={styles.container}>
            <OTPTextView 
                tintColor='coral'
                handleTextChange={text => setOtp(text)}
                containerStyle={'box'}
            />
            <TouchableOpacity
                style={styles.buttonSubmit}
                disabled={loading ? true : false}
                onPress={() => handleSubmission()}
            >
                {loading ?
                    <ActivityIndicator color={'coral'}/>
                    : <Text style={styles.btnText}>Login</Text>
                }
            </TouchableOpacity>
            {/* <CountdownCircleTimer
                size={100}
                isPlaying
                duration={10}
                strokeWidth={8}
                colors={['#004777', '#F7B801', '#A30000', '#A30000']}
                colorsTime={[7, 5, 2, 0]}
            >
                {({ remainingTime }) => <Text style={{fontWeight: '700', fontSize: 18}}>{remainingTime}</Text>}
            </CountdownCircleTimer> */}
        </View>
    )
}

export default OTPVerificationView

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        width: '90%',
        borderRadius: 4,
        backgroundColor: 'white'
    },
    buttonSubmit: {
        height: 45,
        width: '90%',
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: 'coral',
        borderRadius: 4,
        elevation: 4,
        marginVertical: 30
    },
    btnText: {
        color: "#FFF",
        fontSize: 15,
        fontWeight: "500"
    }
})