import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/native";
import React, { useRef, useState } from "react";
import { TouchableOpacity } from "react-native";
import { ActivityIndicator, Text } from "react-native";
import { StyleSheet, View } from "react-native";
import { TextInput } from "react-native-paper";
import PhoneInput from "react-native-phone-number-input";
import Toast from 'react-native-toast-message';

const PhoneNumberView = () => {

    const phoneInput = useRef(null);
    const navigation = useNavigation()
    const [number, setNumber] = useState("")
    const [formattedValue, setFormattedValue] = useState("")
    const [loading, setLoading] = useState(false)
    const [sessionId, setSessionId] = useState()
    
    function generateSessionId() {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < 24; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const handleSubmission = async () => {
        const checkValid = phoneInput.current?.isValidNumber(number);
        if(!checkValid) {
            Toast.show({
                type: 'error',
                text1: 'Invalid phone number!',
            })
        } else {
            setLoading(true)
            try {
                var res = generateSessionId()
                console.log(res)
                await AsyncStorage.setItem('phoneNumber', JSON.stringify(number))
                await AsyncStorage.setItem('sessionId', JSON.stringify(res))
                    .then(() => navigation.navigate('OTPVerificationView'))
                    .catch((e) => console.log(e))
            } catch (e) {
                console.log(e)
                Toast.show({
                    type: 'error',
                    text1: 'Unexpected error occured',
                })
            } finally {
                setLoading(false)
            }
        }
    }

    return (
        <View style={styles.container}>
            <Text style={{fontSize: 24, fontWeight: '700', marginBottom: 30}}>OTP Login</Text>
            {/* <TextInput
                style={styles.textInput}
                mode='outlined'
                placeholder="Enter phone number"
                keyboardType="number-pad"
                activeOutlineColor='coral'
                value={number}
                onChangeText={number => setNumber(number)}
            /> */}
            <PhoneInput
                containerStyle={{width: '90%'}}
                ref={phoneInput}
                defaultValue={number}
                defaultCode="IN"
                layout="first"
                onChangeText={(text) => setNumber(text)}
                onChangeFormattedText={(text) => setFormattedValue(text)}
                withShadow
                autoFocus
            />
            <TouchableOpacity
                style={styles.buttonSubmit}
                disabled={loading ? true : false}
                onPress={() => handleSubmission()}
            >
                {loading ?
                    <ActivityIndicator color={'coral'}/>
                    : <Text style={styles.btnText}>Get OTP</Text>
                }
            </TouchableOpacity>
        </View>
    )
}

export default PhoneNumberView

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    buttonSubmit: {
        height: 50,
        width: '90%',
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: 'coral',
        borderRadius: 4,
        elevation: 4,
        marginTop: 30
    },
    btnText: {
        color: "#FFF",
        fontSize: 15,
        fontWeight: "500"
    }
})