import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { Button } from "react-native";
import { Text } from "react-native";
import { Alert } from "react-native";
import { View } from "react-native";
import Toast from 'react-native-toast-message';

const ProfileView = () => {

    const navigation = useNavigation()
    const [loading, setLoading] = useState(false)

    const logout = async () => {
        setLoading(true)
        const keys = ['sessionId', 'phoneNumber', 'verified']
        try {
            await AsyncStorage.multiRemove(keys)
            .then(async () => {
                navigation.reset({
                    index: 0,
                    routes: [{name: 'AuthNavigator'}]
                })
                return true
            })
        } catch (err) {
            Toast.show({
                type: 'error',
                text1: 'Unexpected error occured!',
            })
            return false
        } finally {
            setLoading(false)
        }
    }

    const logoutAlert = () => {
        Alert.alert(
            "Confirmation", "Do you want to logout?",
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log("Cancel"),
                    style: "cancel",
                },
                {
                    text: 'Yes',
                    onPress: () => logout()
                }
            ],
            {cancelable: true}
        )
    }

    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>ProfileView</Text>
            <Button title="Logout" onPress={() => logoutAlert()} />
        </View>
    )
}

export default ProfileView