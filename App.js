import "react-native-gesture-handler";
import { StatusBar } from 'expo-status-bar';
import { NavigationContainer } from "@react-navigation/native";
import AuthNavigator from "./src/routes/AuthNavigator";
import Toast from 'react-native-toast-message';
import { useEffect, useState } from "react";
import MainNavigator from "./src/routes/MainNavigator";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function App() {

  const [verified, setVerified] = useState(null)

  const isVerified = async () => {
    try {
      const verified = await AsyncStorage.getItem('verified')
      console.log(await AsyncStorage.getAllKeys())
      setVerified(verified)
    } catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    isVerified()
  }, [verified])

  console.log(verified)

  return (
    <>
      <NavigationContainer>
        <StatusBar style="dark" />
        {verified ? <MainNavigator /> : <AuthNavigator />}
      </NavigationContainer>
      <Toast />
    </>
  );
}